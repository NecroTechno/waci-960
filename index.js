import init from "./pkg/waci_960.js";

const runWasm = async () => {
  const waci960 = await init("./pkg/waci_960_bg.wasm");
  waci960.start();
};
runWasm();
