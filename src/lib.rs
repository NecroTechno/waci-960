extern crate console_error_panic_hook;
extern crate instant;

use instant::Instant;
use js_sys::Date;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen::JsValue;

use std::cell::RefCell;
use std::f64;
use std::panic;
use std::rc::Rc;
use std::sync::{Arc, Mutex};

mod utils;

use utils::RenderAxis;

static TEXT_RENDER_PADDING: f64 = 15.0;

struct Shell {
    window: web_sys::Window,
    document: web_sys::Document,
    canvas: web_sys::HtmlCanvasElement,
    context: web_sys::CanvasRenderingContext2d,
    characters: Vec<u8>,
    width: usize,
    height: usize,
    cols: usize,
    rows: usize,
    pos: usize,
    key_down_listener: Option<web_sys::EventListener>,
    command: Vec<u8>,
    current_directory: String,
    last_render_timestamp: Option<Instant>,
    render_options: utils::RenderOptions,
}

impl Shell {
    fn init(&mut self) {
        self.canvas.set_attribute("width", &self.width.to_string());
        self.canvas
            .set_attribute("height", &self.height.to_string());
    }

    fn print(&mut self, s: &str) {
        let cs: Vec<char> = s.chars().collect();
        for i in 0..cs.len() {
            let c = cs[i] as u8;
            self.characters[self.pos] = c;
            if c == 10 {
                self.pos = (self.pos / self.cols + 1) * self.cols;
            } else {
                self.pos += 1;
            }
        }
        self.render();
    }

    fn get_render_size(&self, render_axis: RenderAxis, size: usize) -> f64 {
        match render_axis {
            RenderAxis::Width => (self.width as f64 / self.cols as f64) * size as f64,
            RenderAxis::Height => (self.height as f64 / self.rows as f64) * size as f64,
        }
    }

    fn render_info_bar(&mut self) {
        self.context.set_fill_style(&JsValue::from_str("#b98eff"));
        self.context.fill_rect(
            0.0,
            0.0,
            self.get_render_size(RenderAxis::Width, self.width),
            self.get_render_size(RenderAxis::Height, 1),
        );

        self.context.set_fill_style(&JsValue::from_str("#2a2a2e"));
        self.context.set_font("16px monospace");

        let current_date = utils::jstime_to_date(Date::new_0());

        let fps = utils::calculate_fps(self.last_render_timestamp);

        let fps_info = format!("FPS: {:.2}", fps);

        let info_characters: Vec<char> = format!("{} {}", current_date, fps_info).chars().collect();

        for x in 0..info_characters.len() {
            self.context.fill_text(
                &(info_characters[x] as char).to_string(),
                self.get_render_size(RenderAxis::Width, x),
                TEXT_RENDER_PADDING + self.get_render_size(RenderAxis::Height, 0),
            );
        }
    }

    // TODO: Corners?
    fn render_border(&mut self) {
        self.context.set_fill_style(&JsValue::from_str("#66A05B"));
        self.context.set_font("16px monospace");

        let border_row_start_index = match self.render_options.info_bar {
            true => 1,
            false => 0,
        };

        for y in 0..self.rows - border_row_start_index {
            self.context.fill_text(
                "║",
                self.get_render_size(RenderAxis::Width, 0),
                TEXT_RENDER_PADDING
                    + self.get_render_size(RenderAxis::Height, y + border_row_start_index),
            );
        }
        for y in 0..self.rows - border_row_start_index {
            self.context.fill_text(
                "║",
                self.get_render_size(RenderAxis::Width, self.cols - 1),
                TEXT_RENDER_PADDING
                    + self.get_render_size(RenderAxis::Height, y + border_row_start_index),
            );
        }
        for x in 0..self.cols {
            self.context.fill_text(
                "═",
                self.get_render_size(RenderAxis::Width, x),
                TEXT_RENDER_PADDING
                    + self.get_render_size(RenderAxis::Height, border_row_start_index),
            );
        }
        for x in 0..self.cols {
            self.context.fill_text(
                "═",
                self.get_render_size(RenderAxis::Width, x),
                TEXT_RENDER_PADDING + self.get_render_size(RenderAxis::Height, self.rows - 1),
            );
        }

        let title_characters: Vec<char> = "WACI-960".chars().collect();

        self.context.set_fill_style(&JsValue::from_str("#66A05B"));
        self.context.fill_rect(
            self.get_render_size(RenderAxis::Width, 1),
            self.get_render_size(RenderAxis::Height, border_row_start_index),
            self.get_render_size(RenderAxis::Width, title_characters.len() + 2),
            self.get_render_size(RenderAxis::Height, 1), //this should be varaible...
        );

        self.context.set_fill_style(&JsValue::from_str("#2a2a2e"));
        self.context.set_font("16px monospace");

        for x in 0..title_characters.len() {
            self.context.fill_text(
                &(title_characters[x] as char).to_string(),
                self.get_render_size(RenderAxis::Width, x + 2),
                TEXT_RENDER_PADDING
                    + self.get_render_size(RenderAxis::Height, border_row_start_index),
            );
        }
    }

    // render text from character vec
    fn render_term_history(&mut self) {
        let (x_axis_buffer_left, x_axis_buffer_right) = match self.render_options.info_bar {
            true => (1, 1),
            false => (0, 0),
        };
        let (y_axis_buffer_top, y_axis_buffer_bottom) = match self.render_options {
            utils::RenderOptions {
                info_bar: true,
                border: true,
            } => (2, 1),
            utils::RenderOptions {
                info_bar: true,
                border: false,
            } => (1, 0),
            utils::RenderOptions {
                info_bar: false,
                border: true,
            } => (1, 0),
            utils::RenderOptions {
                info_bar: false,
                border: false,
            } => (0, 0),
        };
        self.context.set_fill_style(&JsValue::from_str("#FFFFFF"));
        self.context.set_font("16px monospace");
        for x in x_axis_buffer_left..(self.cols - x_axis_buffer_right) {
            for y in y_axis_buffer_top..(self.rows - y_axis_buffer_bottom) {
                self.context.fill_text(
                    &(self.characters[((y - y_axis_buffer_top) * (self.cols - y_axis_buffer_top))
                        + (x - x_axis_buffer_right)] as char)
                        .to_string(),
                    self.get_render_size(RenderAxis::Width, x),
                    TEXT_RENDER_PADDING + self.get_render_size(RenderAxis::Height, y),
                );
            }
        }
    }

    fn render(&mut self) {
        // render background
        self.context.set_fill_style(&JsValue::from_str("#2a2a2e"));
        self.context
            .fill_rect(0.0, 0.0, self.width as f64, self.height as f64);

        if self.render_options.info_bar {
            self.render_info_bar();
        }

        if self.render_options.border {
            self.render_border();
        }

        self.render_term_history();

        self.last_render_timestamp = Some(Instant::now());
    }

    fn event_loop(&mut self) {
        self.render();
    }
}

fn setup_event_listeners(shell: &Arc<Mutex<Shell>>) {
    let mut shell_listener_clone = shell.clone();

    let event_listener_closure = Closure::wrap(Box::new(move |event: web_sys::KeyEvent| {
        //utils::console_log(&format!("{:?}", event.unwrap()));
        //shell_listener_clone.lock().unwrap().print(event.char_code.to_string());
        let keyboard_event = event.clone().dyn_into::<web_sys::KeyboardEvent>().unwrap();

        let mut event_string = String::from("");
        //event_string.push_str(&event.type_());
        //event_string.push_str(&" : ");
        event_string.push_str(&keyboard_event.key());

        shell_listener_clone.lock().unwrap().print(&event_string);
    }) as Box<dyn FnMut(_)>);
    utils::window().add_event_listener_with_callback(
        "keydown",
        event_listener_closure.as_ref().unchecked_ref(),
    );
    event_listener_closure.forget();
}

fn setup_event_loop(shell: &Arc<Mutex<Shell>>) {
    let f = Rc::new(RefCell::new(None));
    let g = f.clone();
    let mut shell_loop_clone = shell.clone();

    *g.borrow_mut() = Some(Closure::wrap(Box::new(move || {
        shell_loop_clone.lock().unwrap().event_loop();
        utils::request_animation_frame(f.borrow().as_ref().unwrap());
    }) as Box<dyn FnMut()>));

    utils::request_animation_frame(g.borrow().as_ref().unwrap());
}

#[wasm_bindgen(start)]
pub fn start() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let canvas = document
        .get_element_by_id("canvas")
        .unwrap()
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .map_err(|_| ())
        .unwrap();
    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    let width = 960;
    let height = 720;
    let cols = 100;
    let rows = 38;

    let mut shell = Arc::new(Mutex::new(Shell {
        window,
        document,
        canvas,
        context,
        characters: vec![32; cols * rows],
        width,
        height,
        cols,
        rows,
        pos: 0,
        key_down_listener: None,
        command: vec![0],
        current_directory: "/".to_string(),
        last_render_timestamp: None,
        render_options: utils::RenderOptions {
            info_bar: true,
            border: true,
        },
    }));

    shell.lock().unwrap().init();

    setup_event_listeners(&shell);
    setup_event_loop(&shell);
}
