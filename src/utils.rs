extern crate chrono;
extern crate instant;

use chrono::prelude::*;
use instant::Instant;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen::JsValue;
use web_sys::console;

use std::any::Any;

pub enum RenderAxis {
    Width,
    Height,
}

pub struct RenderOptions {
    pub info_bar: bool,
    pub border: bool,
}

pub fn console_log(logged: &dyn Any) {
    if let Some(string) = logged.downcast_ref::<String>() {
        console::log_1(&JsValue::from_str(&format!("{:?}", string)));
    } else {
        println!("Not a string...");
    }
}

pub fn calculate_fps(render_timestamp: Option<Instant>) -> f64 {
    if render_timestamp.is_some() {
        let millis_since_render = render_timestamp.unwrap().elapsed().as_millis();
        1000.0 / millis_since_render as f64
    } else {
        0.0
    }
}

pub fn jstime_to_date(jstime: js_sys::Date) -> String {
    let datetime: DateTime<Utc> = DateTime::<Utc>::from(jstime);
    let newdate = datetime.format("%Y-%m-%d %H:%M:%S");

    format!("{}", newdate)
}

pub fn window() -> web_sys::Window {
    web_sys::window().expect("no global `window` exists")
}

pub fn request_animation_frame(f: &Closure<dyn FnMut()>) {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK");
}
